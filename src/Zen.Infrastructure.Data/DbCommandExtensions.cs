﻿using System;
using System.Data;
using System.Data.SqlClient;

namespace Zen.Infrastructure.Data
{
    public static class DbCommandExtensions
    {
        public static IDbCommand AddIfNotEmptyOrDbNull(this IDbCommand command, string parameterName, string value)
        {
            var parameter = new SqlParameter { ParameterName = parameterName };
            if (string.IsNullOrEmpty(value))
                parameter.Value = DBNull.Value;
            else
                parameter.Value = value;

            command.Parameters.Add(parameter);
            return command;
        }

        public static IDbCommand AddWithValue(this IDbCommand command, string parameterName, object value)
        {
            var parameter = new SqlParameter
            {
                ParameterName = parameterName,
                Value = value
            };

            command.Parameters.Add(parameter);
            return command;
        }

        public static IDbCommand AddWithDecimalValue(this IDbCommand command, string parameterName, object value, int precision, int scale)
        {
            var parameter = new SqlParameter
            {
                ParameterName = parameterName,
                Value = value,
                Precision = (byte)precision,
                Scale = (byte)scale
            };

            command.Parameters.Add(parameter);
            return command;
        }

        public static IDbCommand AddWithValueOrDbNull(this IDbCommand command, string parameterName, object value)
        {
            var parameter = new SqlParameter
            {
                ParameterName = parameterName,
                Value = value ?? DBNull.Value
            };

            command.Parameters.Add(parameter);
            return command;
        }

        public static IDbCommand AddWithValueOrDbNull<TValue>(this IDbCommand command, string parameterName, TValue value, Func<TValue, bool> predicate)
        {
            var parameter = new SqlParameter { ParameterName = parameterName };
            if (predicate(value))
                parameter.Value = value;
            else
                parameter.Value = DBNull.Value;

            command.Parameters.Add(parameter);
            return command;
        }

        public static IDbCommand AddWithValueOrDbNull<TValue>(this IDbCommand command, string parameterName, TValue? value)
            where TValue : struct
        {
            var parameter = new SqlParameter { ParameterName = parameterName };
            if (value.HasValue)
                parameter.Value = value.Value;
            else
                parameter.Value = DBNull.Value;

            command.Parameters.Add(parameter);
            return command;
        }

        public static IDbCommand AddOutParameter(this IDbCommand command, string parameterName, SqlDbType type)
        {
            var parameter = new SqlParameter
            {
                ParameterName = parameterName,
                SqlDbType = type,
                Direction = ParameterDirection.Output,
            };

            command.Parameters.Add(parameter);
            return command;
        }

        public static IDbCommand AddOutParameter(this IDbCommand command, string parameterName, SqlDbType type, out SqlParameter parameter)
        {
            parameter = new SqlParameter
            {
                ParameterName = parameterName,
                SqlDbType = type,
                Direction = ParameterDirection.Output,
            };

            command.Parameters.Add(parameter);
            return command;
        }
    }
}

