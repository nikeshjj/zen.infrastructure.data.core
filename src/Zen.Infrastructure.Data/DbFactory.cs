using System.Data;
using System.Data.SqlClient;

namespace Zen.Infrastructure.Data
{
    public static class DbFactory
    {
        public static IDbConnection CreateConnection(string connectionString)
        {
            return new SqlConnection(connectionString);
        }
    }
}